<?php
require_once __DIR__ . '/vendor/autoload.php';
use App\Parser;

$maxDepth = 5;

system('clear');

if (isset($argv[1])){
    switch ($argv[1]) {
        case 'parse':
            $result = new Parser($argv[2], $maxDepth);
            echo $result->getReportToFile();
            break;
        case 'report':
            $result = new Parser($argv[2], $maxDepth);
            echo $result->getReportToScreen();
            break;
        case 'help':
            echo "Список доступных команд:" . "\r\n";
            echo "parse [url] - находит картинки на страницах url и сохраняет в файл. Результат выполнения 
            - ссылка на *.csv - файл" . "\r\n";
            echo "report [domain] - находит картинки на страницах url и выводит в консоль" . "\r\n";
            break;
        default:
            echo "Нет такой команды. Введите команду или help для просмотра доступных команд"  . "\n";
    }
} else{
    echo "Вы ничего не ввели. Введите команду или help для просмотра доступных команд"  . "\n";
}


