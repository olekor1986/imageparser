<?php


namespace App;

use Exception;

class Parser
{
    protected $url;
    protected $depth = 5;
    protected $seen = [];
    protected $result = '';
    protected $host;
    protected $path;
    protected $httpCode;

    public function __construct($url, $depth)
    {
        if (preg_match('/^(http|https).+$/', $url)){
            $this->url = $url;
        } else {
            $this->url = 'http://' . $url;
        }
        $this->depth = $depth;
        $parts = parse_url($this->url);
        $this->host = $parts['host'];
        if (!empty($parts['path'])){
            $this->path = $parts['path'];
        }
    }

    /**
     * Save source url and image links to CSV file
     * @param $result
     * @return string
     */
    private function saveToFile($result):string
    {
        $result = "url,image" . "\r\n" . $result;
        $filename = realpath(__DIR__ . '/..') . '/reports/' . $this->host . str_ireplace('/', '-', $this->path) . '.csv';
        $file = fopen($filename, 'w');
        fwrite($file, $result);
        fclose($file);
        return $filename . "\r\n";
    }

    /**
     *  Find matches with image link patterns
     * @param $url
     * @return string
     */
    private function getImagesByURL($url):string
    {
        $linkArray = [];
        $page = $this->getPage($url);
        preg_match_all('/<img.*?src=["\'](.*?)["\'].*?>/is', $page,
            $linkArray, PREG_PATTERN_ORDER);
        $extensionPattern = '/^.*\.(gif|jpg|jpeg|png|svg)$/';
        $images = '';
        foreach ($linkArray[1] as $link){
            $link = stripslashes($link);
            $parts = parse_url($url);
            if (preg_match('/^(http|https).*$/', $link) &&
                preg_match($extensionPattern, $link)) {
                $images .= $url . "," . $link . "\r\n";
            } elseif (preg_match('/^(\/)?(www)\..*$/', $link) &&
                preg_match($extensionPattern, $link)) {
                $images .= $url . "," . $link . "\r\n";
            } elseif (preg_match('/^(\/\/).*$/', $link) &&
                preg_match($extensionPattern, $link)) {
                $images .= $url . "," . $parts['scheme']  . '://' . substr($link, 2) . "\r\n";
            } elseif (preg_match('/^(\/).*$/', $link) &&
                preg_match($extensionPattern, $link)) {
                $images .= $url . "," . $parts['scheme']  . '://' . $parts['host'] . $link . "\r\n";
            }
            if (empty($link)){
                continue;
            }

        }
        return $images;
    }

    /**
     *  Get page string by URL
     * @param $url
     * @return bool|string
     */
    private function getPage($url)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $result = curl_exec($curl);
            $this->httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($result === false) {
                throw new Exception("Curl error! " . curl_error($curl) . "\r\n");
            } else {
                return $result;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Find matches with URL patterns and build URL from parts
     * @param $url
     * @param $depth
     */
    private function getLinks($url, $depth)
    {

        $page = $this->getPage($url);
        $hrefs = [];
        preg_match_all('/<a.*?href=["\'](.*?)["\'].*?>/is',
            $page, $hrefs,PREG_PATTERN_ORDER);
        foreach ($hrefs[1] as $href) {
            if (0 !== strpos($href, 'http')) {
                $path = '/' . ltrim($href, '/');
                if (extension_loaded('http')) {
                    $href = http_build_url($url, array('path' => $path));
                } else {
                    $parts = parse_url($url);
                    $href = $parts['scheme'] . '://';
                    if (isset($parts['user']) && isset($parts['pass'])) {
                        $href .= $parts['user'] . ':' . $parts['pass'] . '@';
                    }
                    $href .= $parts['host'];
                    if (isset($parts['port'])) {
                        $href .= ':' . $parts['port'];
                    }
                    $href .= $path;
                }
            }
            $this->parsePage($href, $depth - 1);
        }

    }

    /**
     * Checking URL and host, checking parsing depth, if URL was checked
     * @param $url
     * @param $depth
     * @return bool
     */
    private function isValid($url, $depth):bool
    {
        if (strpos($url, $this->host) === false || $depth === 0 || isset($this->seen[$url])) {
            return false;
        }
        return true;
    }

    /**
     * If URL valid start parse pages, images by URL add to result
     * @param $url
     * @param $depth
     */
    private function parsePage($url, $depth)
    {
        if (!$this->isValid($url, $depth)) {
            return;
        }
        $this->seen[$url] = true;
        $this->result .= $this->getImagesByURL($url);
        $this->getLinks($url, $depth);
    }


    /**
     * Return CSV file report path
     * @return string
     */
    public function getReportToFile():string
    {
        $this->parsePage($this->url, $this->depth);
        return $this->saveToFile($this->result);
    }

    /**
     * Return report to console
     * @return string
     */
    public function getReportToScreen():string
    {
        $this->parsePage($this->url, $this->depth);
        return 'url,image' . "\r\n" . $this->result;
    }

}
